package org.example;

/**
 * Hello world!
 *
 */
public class PaymentApp
{
    public static void main( String[] args )
    {
        System.out.println( "Welcome to Payment App" );
        System.out.println("=============================");


        PaymentAppObjectFacory facory = new PaymentAppObjectFacory();

        Gateway stripe = facory.getGateway("stripe");
        Gateway paypal = facory.getGateway("paypal");

        PaymentProcessor paymentProcessor = facory.getPaymentProcessor(paypal);
        paymentProcessor.makePayment(1000);
        System.out.println(paymentProcessor.getPaymentStatus(2012));
    }
}

//control the dependent through dependency
//through dependency injection we achieve inversion of control where the dependency tells dependent how it has to function

//objectfactory -- factory design pattern


////        PaypalPaymentGateway gateway = new PaypalPaymentGateway();
//        PaypalPaymentGateway paypal = new PaypalPaymentGateway();
//        StripePaymentGateway stripe = new StripePaymentGateway();
////        PaymentProcessor paymentProcessor = new PaymentProcessor(gateway);
//        PaymentProcessor paymentProcessor = new PaymentProcessor(paypal);
//        paymentProcessor.makePayment(1000);
//        System.out.println(paymentProcessor.getPaymentStatus(2012));

