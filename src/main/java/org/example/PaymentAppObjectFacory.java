package org.example;

public class PaymentAppObjectFacory {

    public Gateway getGateway(String name){

        if(name.equals("paypal")){
            return new PaypalPaymentGateway();
        } else if(name.equals("stripe")){
            return new StripePaymentGateway();
        } else

            return null;
    }

    public PaymentProcessor getPaymentProcessor(Gateway gateway){

        return new PaymentProcessor(gateway);
    }
}


//spring core